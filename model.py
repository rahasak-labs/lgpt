import os
from langchain.chat_models import ChatOpenAI
from langchain.embeddings import OpenAIEmbeddings
from langchain.vectorstores import Chroma
from langchain.chains import ConversationalRetrievalChain, LLMChain
from langchain.memory import ConversationBufferMemory
from langchain.llms import OpenAI
from langchain.memory import MongoDBChatMessageHistory
from langchain.document_loaders.recursive_url_loader import RecursiveUrlLoader
from langchain.text_splitter import RecursiveCharacterTextSplitter
from langchain.prompts import ChatPromptTemplate, HumanMessagePromptTemplate, MessagesPlaceholder, SystemMessagePromptTemplate, PromptTemplate
from langchain.utils.html import (PREFIXES_TO_IGNORE_REGEX,
                                  SUFFIXES_TO_IGNORE_REGEX)
from bs4 import BeautifulSoup as Soup
import logging
import sys
from config import *

logging.basicConfig(stream=sys.stdout, level=logging.INFO, format='%(asctime)s - %(levelname)s - %(message)s')

global conversation
global vectordb
conversation = None
vectordb = None

def init_index():
    if not INIT_INDEX:
        logging.info("continue without initializing index")
        return

    logging.info("initializing index from `%s`", TARGET_URL)

    # scrape data from web
    documents = RecursiveUrlLoader(
        TARGET_URL,
        max_depth=4,
        extractor=lambda x: Soup(x, "html.parser").text,
        prevent_outside=True,
        use_async=True,
        timeout=600,
        check_response_status=True,
        # drop trailing / to avoid duplicate pages.
        link_regex=(
            f"href=[\"']{PREFIXES_TO_IGNORE_REGEX}((?:{SUFFIXES_TO_IGNORE_REGEX}.)*?)"
            r"(?:[\#'\"]|\/[\#'\"])"
        ),
    ).load()

    logging.info("index created with `%d` documents", len(documents))

    # split text
    text_splitter = RecursiveCharacterTextSplitter(chunk_size=4000, chunk_overlap=200)
    spllited_documents = text_splitter.split_documents(documents)

    # create embedding and persist on vector db
    embeddings = OpenAIEmbeddings()
    vectordb = Chroma.from_documents(
        documents=spllited_documents,
        embedding=embeddings,
        persist_directory=INDEX_PERSIST_DIRECTORY
    )
    vectordb.persist()

def init_conversation():
    global vectordb
    global conversation

    # load index
    embeddings = OpenAIEmbeddings()
    vectordb = Chroma(persist_directory=INDEX_PERSIST_DIRECTORY,embedding_function=embeddings)

    # custom proment
    prompt = ChatPromptTemplate(
        messages=[
            SystemMessagePromptTemplate.from_template(
                """
                You are an agent tasked with providing answers about the Open5GS 5G core, based on its official documentation.
                """
            ),
            HumanMessagePromptTemplate.from_template(
                """
                Using the provided Open5GS documentation, please respond to the question below:
                Question: {question}
                Relevant Documents: {input_documents}
                Previous Chat History: {chat_history}
                """
            )
        ],
    )

    # chat with conversational memory
    conversation = LLMChain(
        llm=ChatOpenAI(temperature=0.7, model_name="gpt-4", max_tokens=1024),
        prompt=prompt,
        verbose=True,
    )

    return conversation

def chat(question, user_id):
    global conversation
    global vectordb

    # constrct chat history with mongodb
    # session_id is the user_id which comes through http request
    # mongo connection string (e.g mongodb://localhost:27017/)
    connection_string = f"mongodb://{MONGO_HOST}:{MONGO_PORT}/"
    history = MongoDBChatMessageHistory(
        connection_string=connection_string, session_id=user_id
    )

    # find related documetns to the question
    documents = vectordb.similarity_search(question)
    input_documents = [doc.page_content for doc in documents]
    logging.info("%d input documents", len(input_documents))

    # gpt conversation
    # provide inputs to the proment - question, input_documents, chat_history
    answer = conversation.predict(question=question, input_documents=input_documents, chat_history=history.messages)

    logging.info("got response from llm - %s", answer)

    # save history
    history.add_user_message(question)
    history.add_ai_message(answer)

    return answer
